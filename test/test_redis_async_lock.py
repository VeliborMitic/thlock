import asyncio
import pytest

from thlock import RedisLock


@pytest.mark.asyncio
async def test_lock():
    '''
    Test redis lock functions: acquire, release, close - success expected
    '''
    HOST = 'redis-test'
    PORT = 6379

    # lock
    lock = RedisLock(host=HOST, port=PORT, name='lock-0')
    await lock.acquire()
    await asyncio.sleep(1.0)
    await lock.release()
    await lock.close()


@pytest.mark.asyncio
async def test_lock_is_acquired():
    '''
    Test redis lock functions: acquire, is_acquired, release, close - success expected
    '''
    HOST = 'redis-test'
    PORT = 6379

    # lock
    lock = RedisLock(host=HOST, port=PORT, name='lock-0')
    r = await lock.acquire()
    assert r
    acquired = await lock.is_acquired()
    assert acquired
    await lock.release()
    await lock.close()


@pytest.mark.asyncio
async def test_lock_release():
    '''
    Test redis lock functions: acquire, release, close - success expected
    '''
    HOST = 'redis-test'
    PORT = 6379

    # lock
    lock = RedisLock(host=HOST, port=PORT, name='lock-0')
    await lock.acquire()
    r = await lock.release()
    assert r
    await lock.close()


@pytest.mark.asyncio
async def test_lock_acquire_timeout():
    '''
    Test redis lock functions: acquire with timeout, release, close - success expected
    '''
    HOST = 'redis-test'
    PORT = 6379

    # lock
    lock = RedisLock(host=HOST, port=PORT, name='lock-0')
    await lock.acquire(timeout=10)
    r = await lock.release()
    assert r
    await lock.close()
