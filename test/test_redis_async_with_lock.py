import asyncio
import pytest

from thlock import RedisLock


@pytest.mark.asyncio
async def test_lock():
    '''
    Test async with redis lock - success expected
    '''
    HOST = 'redis-test'
    PORT = 6379

    lock = RedisLock(host=HOST, port=PORT, name='lock-0')

    async with lock:
        await asyncio.sleep(1.0)

    is_acquired: bool = await lock.is_acquired()
    assert not is_acquired

    await lock.close()


@pytest.mark.asyncio
async def test_lock_is_acquired():
    '''
    Test async with redis lock - check if lock is acquired - success expected
    '''
    HOST = 'redis-test'
    PORT = 6379

    lock = RedisLock(host=HOST, port=PORT, name='lock-0')
    
    async with lock:
        acquired = await lock.is_acquired()
        assert acquired
        await asyncio.sleep(1.0)

    # assert that lock is released after exiting the context manager
    is_acquired: bool = await lock.is_acquired()
    assert not is_acquired

    await lock.close()


"""
@pytest.mark.asyncio
async def test_lock_release():
    '''
    Test async with redis lock - release lock - success expected
    '''
    HOST = 'redis-test'
    PORT = 6379

    lock = RedisLock(host=HOST, port=PORT, name='lock-0')
    
    async with lock:
        released = await lock.release()
        assert released
        await asyncio.sleep(1.0)

        # Assert that lock is no more acquired after lock.release()
        acquired = await lock.is_acquired()
        assert not acquired
    
    await lock.close()
"""


@pytest.mark.asyncio
async def test_lock_raise_exception_on_purpose():
    '''
    Test async with redis lock - raise exception on purpose - success expected
    '''
    HOST = 'redis-test'
    PORT = 6379

    lock = RedisLock(host=HOST, port=PORT, name='lock-0')
    
    async with lock:
        a = 1/0
        acquired = await lock.is_acquired()
        assert acquired
        await asyncio.sleep(1.0)
    
    await lock.close()
