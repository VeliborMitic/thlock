import asyncio

from thlock import EtcdLock


# Usage with context manager

HOST = 'etcd-test'
PORT = 2379

# create a lock
lock = EtcdLock(host=HOST, port=PORT, name='lock-0')

# use lock with context manager
# acquire lock on initialization, releases and closes the lock on exit
async with lock:
    await asyncio.sleep(1.0)


