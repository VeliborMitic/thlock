import asyncio
from thlock import EtcdLock


HOST = 'etcd-test'
PORT = 2379

# create a lock
lock = EtcdLock(host=HOST, port=PORT, name='lock-0')

# acquire lock
# function is accepting optional argument 'timeout: float', if not provided default value 10.0 is used
# function is returning bool value which represent success or failure
acquired: bool = await lock.acquire(20)
await asyncio.sleep(5.0)

# check if lock is already acquired
# returns bool
is_acquired: bool = await lock.is_acquired()

# release lock
# function is returning bool value which represent success or failure
released: bool = await lock.release()

# close lock
await lock.close()
